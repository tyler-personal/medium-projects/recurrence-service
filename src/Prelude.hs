{-# LANGUAGE NoMonomorphismRestriction #-}
module Prelude
  ( module Prelude
  , module BasePrelude
  , module Data.Text
  , module Data.Text.Lazy
  , module Data.String.Interpolate
  ) where

import BasePrelude hiding (words, unwords, unlines, toUpper, toTitle, toLower, intercalate,
  id)
import Data.Text          (words, unwords, unlines, toUpper, toTitle, toLower, intercalate,
  Text, unpack)

import Data.Text.Lazy (fromStrict)
import Data.String.Interpolate

readMay text = readMaybe (unpack text)

(∈) = elem
