{-# LANGUAGE QuasiQuotes #-}
module Presentation.Items where

import Domain.Items

class Display a where
  asJson :: a -> Text

maybeAsJson :: Display a => Maybe a -> Text
maybeAsJson (Just item) = asJson item
maybeAsJson Nothing = [i| {"error": "id is not valid"} |]

allAsJson :: (Display a, Foldable t) => t a -> Text
allAsJson xs = [i|[#{items}]|]
  where
    items = intercalate "," . map asJson $ toList xs
