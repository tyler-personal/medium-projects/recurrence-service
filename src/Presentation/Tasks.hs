{-# LANGUAGE QuasiQuotes #-}
module Presentation.Tasks where

import Domain.Tasks
import Presentation.Items (Display, asJson)
import Time

textToFrequency :: Text -> Frequency
textToFrequency text = f (toLower text)
  where
    f "day"   = Day
    f "week"  = Week
    f "month" = Month

maybeTaskTime :: Maybe Text -> Maybe Text -> Maybe TaskTime
maybeTaskTime (Just hour) (Just minute) =
  case (readMay hour, readMay minute) of
    (Just h, Just m) -> Just $ TaskTime (Time @Hour h) (Time @Minute m)
    (_, _)           -> Nothing
maybeTaskTime _ _ = Nothing

instance Display Task where
  asJson Task {..} =
    [i|{
      "id": #{taskID},
      "name": "#{name}",
      "frequency": "#{frequency}",
      "time": "#{time}"
    }|]
    where
      time = case timeOfDay of
        Just t  -> show t
        Nothing -> "N.A"
