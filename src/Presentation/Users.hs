{-# LANGUAGE QuasiQuotes #-}
module Presentation.Users where

import Domain.Users
import Presentation.Tasks
import Presentation.Items

instance Display User where
  asJson User {..} =
    [i|{
      "id": #{userID},
      "email": "#{email}",
      "tasks": [tasksJson]
    }|]
    where
      tasksJson = allAsJson tasks
