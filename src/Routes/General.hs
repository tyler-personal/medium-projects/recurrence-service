{-# LANGUAGE QuasiQuotes #-}
module Routes.General
  ( module Routes.General
  , module Web.Scotty
  ) where

import Web.Scotty

safeParam :: Parsable a => Text -> ActionM (Maybe a)
safeParam x = do
  p <- param (fromStrict x)
  return (Just p) `rescue` (\_ -> return Nothing)

resultJson :: Text -> Text -> ActionM ()
resultJson status jsonData = json @Text [i|{"status": "#{status}", "data": #{jsonData}}|]
