{-# LANGUAGE QuasiQuotes #-}
module Routes.Users where

import Domain.Tasks
import Domain.Users

import Presentation.Items (asJson)

import Routes.General

createUserRoute :: UserRepository -> ActionM ()
createUserRoute repo = do
  id <- param "id"
  email <- param "email"
  password <- param "password"
  let tasks = [] :: [Task]
  user <- newUser repo email password tasks
  resultJson "success" [i|{"user": #{asJson user}}|]

