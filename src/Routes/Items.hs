module Routes.Items where

import Domain.Items
import Presentation.Items

import Routes.General

getItemRoute :: Display a => ItemRepository a -> ActionM ()
getItemRoute repo = do
  id <- param "id"
  item <- getItem repo id
  resultJson "success" $ maybeAsJson item

