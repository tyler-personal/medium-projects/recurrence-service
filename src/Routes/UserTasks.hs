{-# LANGUAGE QuasiQuotes #-}
module Routes.UserTasks
  ( getAllUserTasksRoute
  , createUserTaskRoute
  ) where

import Domain.Tasks (TaskRepository, newTask)
import Domain.Items (getAllItems, getItem)
import Domain.Users (UserRepository, User)
import Domain.UserTasks (saveUserTask)

import Presentation.Tasks (maybeTaskTime, textToFrequency)
import Presentation.Items (allAsJson, asJson)
import Presentation.Users

import Routes.General

getAllUserTasksRoute :: UserRepository -> TaskRepository -> ActionM ()
getAllUserTasksRoute userRepo taskRepo = do
  userID <- param "userID"
  maybeUser <- getItem userRepo userID

  tasks <- getAllItems taskRepo
  json $ allAsJson tasks

createUserTaskRoute :: UserRepository -> TaskRepository -> ActionM ()
createUserTaskRoute userRepo taskRepo = do
  name <- param "name"
  frequency <- textToFrequency <$> param "frequency"
  userID <- param "userID"

  maybeHour <- safeParam "hour"
  maybeMinute <- safeParam "minute"
  let timeOfDay = maybeTaskTime maybeHour maybeMinute

  maybeUser <- getItem userRepo userID
  maybeUserAsJson maybeUser $ \user -> do
    task <- newTask taskRepo name frequency timeOfDay
    saveUserTask userRepo taskRepo user task
    resultJson "success" [i|{"task": #{asJson task}, "user": #{asJson user}}|] -- TODO: add user

-- PRIVATE

maybeUserAsJson :: Maybe User -> (User -> ActionM ()) -> ActionM ()
maybeUserAsJson (Just user) f = f user
maybeUserAsJson Nothing f = resultJson "error" [i|{"message": "userID not valid"}|]

