module Main where

import Web.Scotty

import Data.Items
import Domain.Users

import Routes.Items
import Routes.Users
import Routes.UserTasks

main = do
  taskRepo <- memoryStore
  userRepo <- memoryStore

  scotty 3000 $ do
    taskRoutes taskRepo
    userRoutes userRepo
    userTaskRoutes taskRepo userRepo

taskRoutes repo = do
  get "/tasks/:id" $ getItemRoute repo

userRoutes repo = do
  get "/users/:id" $ getItemRoute repo
  post "/users" $ createUserRoute repo

userTaskRoutes taskRepo userRepo = do
  get "/users/:userid/tasks" $ getAllUserTasksRoute userRepo taskRepo
  post "/users/:userid/tasks" $ createUserTaskRoute userRepo taskRepo
