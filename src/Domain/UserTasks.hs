module Domain.UserTasks where

import Domain.Users
import Domain.Tasks
import Domain.Items (saveItem)

saveUserTask :: MonadIO m => UserRepository -> TaskRepository -> User -> Task -> m ()
saveUserTask userRepo taskRepo user task = do
  saveItem taskRepo task
  saveItem userRepo updatedUser

  where
    updatedUser = user { tasks = tasks user ++ [task] }

