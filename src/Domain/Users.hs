module Domain.Users where

import Domain.Tasks (Task)
import Domain.Items

data User = User
  { userID :: Int
  , email :: String
  , password :: String
  , tasks :: [Task]
  }

instance Saveable User where
  id User {..} = userID

type UserRepository = ItemRepository User

newUser repo email password tasks = do
  userID <- generateNewID repo
  return User {userID, email, password, tasks}

