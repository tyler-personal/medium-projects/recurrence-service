module Domain.Items where

import System.Random (randomIO)

data ItemRepository a = ItemRepository
  { _getIds :: IO [Int]
  , _saveItem :: a -> IO ()
  , _getItem :: Int -> IO (Maybe a)
  }

class Saveable a where
  id :: a -> Int

getIds :: MonadIO m => ItemRepository a -> m [Int]
getIds = liftIO . _getIds

saveItem :: MonadIO m => ItemRepository a -> a -> m ()
saveItem = liftIO ./. _saveItem

getItem :: MonadIO m => ItemRepository a -> Int -> m (Maybe a)
getItem = liftIO ./. _getItem

getItems :: (Foldable t, MonadIO m) => ItemRepository a -> t Int -> m [a]
getItems repo ids = catMaybes <$> mapM (getItem repo) (toList ids)

getAllItems :: MonadIO m => ItemRepository a -> m [a]
getAllItems repo = getIds repo >>= getItems repo

generateNewID :: MonadIO m => ItemRepository a -> m Int
generateNewID repo = do
  newNum <- liftIO randomIO
  ids <- getIds repo

  if newNum ∈ ids
    then return newNum
    else generateNewID repo

(./.) = (.) . (.)
