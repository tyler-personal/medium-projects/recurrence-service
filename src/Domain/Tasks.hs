module Domain.Tasks where

import Domain.Items
import Time (Time, Hour, Minute)

data Task = Task
  { taskID          :: Int
  , name            :: Text
  , frequency       :: Frequency
  , timeOfDay       :: Maybe TaskTime
  }

data Frequency = Day | Week | Month
  deriving (Show)

data TaskTime = TaskTime (Time Hour) (Time Minute)
  deriving (Show)

instance Saveable Task where
  id = taskID

type TaskRepository = ItemRepository Task

newTask :: MonadIO m => TaskRepository -> Text -> Frequency -> Maybe TaskTime -> m Task
newTask taskRepo name frequency timeOfDay = do
  taskID <- generateNewID taskRepo
  return Task {taskID, name, frequency, timeOfDay}
