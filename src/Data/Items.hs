module Data.Items where

import Prelude hiding (id) -- this isn't necessary for the compiler, but without it my tooling confuses `id`s and suggests invalid improvements.

import Domain.Items

memoryStore :: Saveable a => IO (ItemRepository a)
memoryStore = newIORef [] >>= \ref ->
  return ItemRepository
    { _getIds = map id <$> readIORef ref
    , _saveItem = \x -> modifyIORef ref (\xs -> xs ++ [x])
    , _getItem = \_id -> find (\x -> id x == _id) <$> readIORef ref
    }
